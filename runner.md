package movie.world.movieassignment.runners;

import movie.world.movieassignment.models.domain.Character;
import movie.world.movieassignment.models.domain.Franchise;
import movie.world.movieassignment.models.domain.Movie;
import movie.world.movieassignment.repositories.CharacterRepository;
import movie.world.movieassignment.repositories.FranchiseRepository;
import movie.world.movieassignment.repositories.MovieRepository;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;


@Component
public class Runner implements ApplicationRunner {


    private final CharacterRepository characterRepository;

    private final MovieRepository movieRepository;
    private final FranchiseRepository franchiseRepository;

    public Runner(CharacterRepository characterRepository, MovieRepository movieRepository, FranchiseRepository franchiseRepository){
        this.characterRepository = characterRepository;
        this.movieRepository = movieRepository;
        this.franchiseRepository = franchiseRepository;
    }


@Override
@Transactional
public void run(ApplicationArguments args) throws Exception {
Franchise franchise = new Franchise(
"Marvel Cinematic Universe",
"no description"

            );
//    Franchise marvel = new Franchise("")
//
//    characterRepository.save(scarlett);

    }
}
