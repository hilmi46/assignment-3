
# Create a Web API and database with Spring.

This project is a PostgreSQL database using Hibernate. 


## Features

- The database stores information about characters, movies they appear in, and the franchises these movies belong to.
- There is a full CRUD is for Movies, Characters, and Franchises. It does deletes, ensures related data is not deleted – the foreign keys can be set to null . 
- dedicated endpoints to:
  Updating characters in a movie.


## Install

This project was generated with OpenJDK version 17.0.2 through Spring Initializr and Gradle build system.
Clone repository via git clone.
Use the Web API through Swagger.


## Maintainers

@hilmi46


## Contributing

Contributions are always welcome!

See `contributing.md` for ways to get started.

Please adhere to this project's `code of conduct`.


## Acknowledgements

 -  My teacher @NicholasLennox gave us lessons about creating a Web API and databases through Spring. 
  

## 🚀 About Me
I'm a Java full stack trainee at the Experis Academy.


## License

[MIT](https://choosealicense.com/licenses/mit/) 2022 Ahmet Hilmi Terzi

