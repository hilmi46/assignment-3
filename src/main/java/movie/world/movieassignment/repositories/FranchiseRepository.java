package movie.world.movieassignment.repositories;

import movie.world.movieassignment.models.Franchise;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FranchiseRepository extends JpaRepository <Franchise, Integer> {
}
