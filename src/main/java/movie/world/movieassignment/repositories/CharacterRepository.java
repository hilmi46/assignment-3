package movie.world.movieassignment.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import movie.world.movieassignment.models.Character;
import org.springframework.stereotype.Repository;


@Repository
public interface CharacterRepository extends JpaRepository <Character, Integer> {
}
