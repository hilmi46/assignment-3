package movie.world.movieassignment.controllers;

import movie.world.movieassignment.models.Character;
import movie.world.movieassignment.services.character.CharacterService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;


@RestController
@RequestMapping(path = "api/v1/characters")

public class CharacterController {

    private final CharacterService characterService;

    public CharacterController(CharacterService characterService) {
        this.characterService = characterService;
    }

    @GetMapping
    public ResponseEntity findAll() {
        return ResponseEntity.ok(characterService.findAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity findById(@PathVariable int id) {return ResponseEntity.ok(characterService.findById(id));
    }
    @PostMapping
    public ResponseEntity add(@RequestBody Character charr) {
        Character newCHar = characterService.add(charr);
        URI uri =  URI.create("movies/" + newCHar.getId());
        return ResponseEntity.created(uri).build();
    }















}

