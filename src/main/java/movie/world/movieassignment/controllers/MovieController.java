package movie.world.movieassignment.controllers;

import movie.world.movieassignment.models.Movie;
import movie.world.movieassignment.services.movie.MovieService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Collection;

@RestController
@RequestMapping(path = "api/v1/movies")
public class MovieController {

    private final MovieService movieService;

    //
    public MovieController(MovieService movieService) {
        this.movieService = movieService;
    }

    @GetMapping
    public ResponseEntity findAll() {
        return ResponseEntity.ok(movieService.findAll());
    }


    @GetMapping("/{id}")
    //Add exception if the searched Id is not found
    public ResponseEntity findById(@PathVariable int id) {
        return ResponseEntity.ok(movieService.findById(id));
    }
    @PostMapping
    public ResponseEntity add(@RequestBody Movie mov) {
        Movie newMov = movieService.add(mov);
        URI uri =  URI.create("movies/" + newMov.getId());
        return ResponseEntity.created(uri).build();
    }


}


