package movie.world.movieassignment.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

import java.util.Set;


@Getter
@Setter
@Entity
@Table(name = "tb_character")
public class Character {
   @Id
   @GeneratedValue(
            strategy = GenerationType.IDENTITY)
    private int id;


   //String full name↓

    @Column(name = "full_name",
            nullable = false,
            length = 100)
    private String fullName;


 //String Alias (can be null)↓
 @Column(name = "alias",
         nullable = true,
         length = 50
  )

 private String alias;

@Column(name = "gender",
        nullable = false,
        length = 50
)
private String gender;

@Column(name = "picture_url",
        nullable = false,
        columnDefinition = "TEXT"
)

    private String pictureUrl;


 // Relationships
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "movie_id")
    @JsonBackReference
    private Movie movie;

    public Character() {

    }


}

