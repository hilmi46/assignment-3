package movie.world.movieassignment.models;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;
@Getter
@Setter
@Entity

@Table(name = "tb_movie")
public class Movie {

    @Id
    @GeneratedValue(
            strategy = GenerationType.IDENTITY
    )


    private int id;

    @Column(name = "movie_title",
            nullable = true,
            length = 100)

    private String title;

    @Column(name = "genre",
            nullable = true,
            length = 100)
    private String genre;
    @Column(name = "release_date",
            nullable = true)

    private int releaseDate;

    @Column(name = "director",
            nullable = true,
            length = 100)

    private String director;

    @Column(name = "url",
            nullable = true,
            columnDefinition = "TEXT")

    private String url;

    @Column(name = "trailer",
            nullable = true,
            columnDefinition = "TEXT")

    private String trailer;

    //Relationships

 @ManyToMany
 @JsonManagedReference
 private Set<Character> characters;
//@ManyToOne
//    @JoinColumn(name = "franchise_id")
    @ManyToOne
    @JoinColumn(name = "franchise_id")
    private Franchise franchise;

}

