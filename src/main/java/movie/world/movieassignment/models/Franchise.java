package movie.world.movieassignment.models;
import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;

import java.util.Set;

@Entity
@Table(name = "tb_franchise")
public class Franchise {
    @Id
    @GeneratedValue(
            strategy = GenerationType.IDENTITY
    )
    private int id;
// String name ↓
    @Column(name = "name",
    nullable = false,
    columnDefinition = "TEXT")
    private String Name;

// String Description ↓
    @Column(name = "description",
            columnDefinition = "TEXT")
    private String description;


    @OneToMany(mappedBy = "franchise")
    @JsonBackReference
    private Set<Movie> movies;


    public Franchise() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<Movie> getMovies() {
        return movies;
    }

    public void setMovies(Set<Movie> movies) {
        this.movies = movies;
    }

}
