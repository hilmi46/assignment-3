package movie.world.movieassignment;

import movie.world.movieassignment.models.Movie;
import movie.world.movieassignment.services.movie.MovieService;
import movie.world.movieassignment.services.movie.MovieServiceImpl;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import java.util.Collection;

@SpringBootApplication

public class MovieAssignmentApplication {

    public static void main(String[] args) {
        SpringApplication.run(MovieAssignmentApplication.class, args);
    }
}





