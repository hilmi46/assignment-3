package movie.world.movieassignment.services.character;

import movie.world.movieassignment.models.Character;
import movie.world.movieassignment.repositories.CharacterRepository;
import movie.world.movieassignment.services.movie.MovieServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collection;

@Service
public class CharacterServiceImpl implements CharacterService {

    private final CharacterRepository characterRepository;
    private final Logger logger = LoggerFactory.getLogger(MovieServiceImpl.class);

    public CharacterServiceImpl(CharacterRepository characterRepository) {
        this.characterRepository = characterRepository;
    }


    @Transactional
    @Override
    public void deleteById(Integer id) {
//        if (characterRepository.existsById(id)) {
//            Character carr = characterRepository.findById(id).get();
//            characterRepository.delete(carr);
//
//        } else
//            logger.warn("No Character exists with ID: " + id);
    }

    @Override
    public boolean exists(Integer id) {
        return characterRepository.existsById(id);
    }


    @Override
    public Character findById(Integer id)  {
            return characterRepository.findById(id).get();
    }

    @Override
    public Collection<Character> findAll() {
        return characterRepository.findAll();

    }

    @Override
    public Character add(Character entity) {
        return characterRepository.save(entity);
    }

    @Override
    public Character update(Character entity) {
        return characterRepository.save(entity);
    }
}

