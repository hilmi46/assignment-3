package movie.world.movieassignment.services.character;

import movie.world.movieassignment.models.Movie;
import movie.world.movieassignment.services.CrudService;
import movie.world.movieassignment.models.Character;

public interface CharacterService extends CrudService<Character, Integer> {

}

/////

//////////