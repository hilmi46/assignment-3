package movie.world.movieassignment.services.franchise;

import movie.world.movieassignment.models.Franchise;
import movie.world.movieassignment.services.CrudService;

public interface FranchiseService extends CrudService <Franchise, Integer> {
}
