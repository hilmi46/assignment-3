package movie.world.movieassignment.services.franchise;

import movie.world.movieassignment.models.Franchise;
import movie.world.movieassignment.repositories.CharacterRepository;
import movie.world.movieassignment.repositories.FranchiseRepository;

import java.util.Collection;

public class FranchiseServiceImpl implements FranchiseService {

    private final FranchiseRepository franchiseRepository;

    public FranchiseServiceImpl(FranchiseRepository franchiseRepository) {
        this.franchiseRepository = franchiseRepository;
    }

    @Override
    public Franchise findById(Integer integer) {
        return null;
    }

    @Override
    public Collection<Franchise> findAll() {
        return null;
    }

    @Override
    public Franchise add(Franchise entity) {
        return null;
    }

    @Override
    public Franchise update(Franchise entity) {
        return franchiseRepository.save(entity);

    }


    @Override
    public void deleteById(Integer id) {
        franchiseRepository.deleteById(id);
    }

    @Override
    public boolean exists(Integer integer) {
        return false;
    }
}
