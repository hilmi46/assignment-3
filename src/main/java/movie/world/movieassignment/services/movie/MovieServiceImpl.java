package movie.world.movieassignment.services.movie;

import movie.world.movieassignment.models.Movie;
import movie.world.movieassignment.repositories.MovieRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collection;

@Service
public class MovieServiceImpl implements MovieService {

    private final MovieRepository movieRepository;
    private final Logger logger = LoggerFactory.getLogger(MovieServiceImpl.class);

    public MovieServiceImpl(MovieRepository movieRepository) {
        this.movieRepository = movieRepository;
    }


    @Transactional
    @Override
    public void deleteById(Integer id) {

    }

    @Override
    public boolean exists(Integer id) {
        return movieRepository.existsById(id);
    }

    public Movie findById(Integer id) {
        return movieRepository.findById(id).get();
//                .orElseThrow(() -> new MovieNotFoundException(id));

    }

    @Override
    public Collection<Movie> findAll() {
        return movieRepository.findAll();
    }

    @Override
    public Movie add(Movie entity) {
        return movieRepository.save(entity);
    }

    @Override
    public Movie update(Movie entity) {
        return movieRepository.save(entity);
    }
}


