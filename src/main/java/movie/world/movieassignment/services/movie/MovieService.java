package movie.world.movieassignment.services.movie;

import movie.world.movieassignment.models.Movie;
import movie.world.movieassignment.services.CrudService;

public interface MovieService extends CrudService<Movie, Integer> {

}
